# InfoTrack SEO Rank Tracker

Web-based application to list the SEO rankings for Google and Bing.
Search is based on keyword "online title search". It checks the position of infotrack.com.au on the list. 

It checks the first 50 list only.

## Installation

Install .Net Core packages

Install  reactjs app packages


## Architecture

Application is built on Clean Architecture. 

With Clean Architecture, the Domain and Application layers are at the centre of the design. This is known as the Core of the system. Web and Infrastructure are the remaining layers.

## Technologies
ASP .Net Core 3.1

Reactjs

MediatR

