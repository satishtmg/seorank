﻿using Application.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Infrastructure.Services
{
    public class GoogleService : ISearchEngineService
    {
        private readonly IWebPageService _webPageService;
        private readonly IRegexService _regexService;
        private readonly IConfiguration _configuration;
        public GoogleService(IWebPageService webPageService, IRegexService regexService, IConfiguration configuration)
        {
            _webPageService = webPageService;
            _regexService = regexService;
            _configuration = configuration;
        }
        public string CountRank(string keyword)
        {
            try
            {
                List<int> rank = new List<int>();

                int totalResult = 0;

                for (int pageNumber = 1; pageNumber <= Int32.Parse(_configuration.GetSection("SearchPageSize").Value); pageNumber++)
                {
                    string prefix = pageNumber > 9 ? "" : "0";

                    var matchedCollection = _regexService.GetMatchedData(@"<[dD][iI][vV]\sclass=[\""\']?g[\""\']?>([^$]+?)<\/div>",
                   _webPageService.DownloadHtml("https://infotrack-tests.infotrack.com.au/Google/Page" + prefix + pageNumber + ".html"));

                    foreach (var item in matchedCollection)
                    {
                        var a = item.ToString();
                        if (a.Contains("infotrack.com.au"))
                        {
                            rank.Add(totalResult + 1);
                        }
                        totalResult++;
                    }
                }

                if (rank.Count > 0)
                {
                    return String.Join(",", rank);
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
