﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum SearchEngineType
    {
        Google = 1,
        Bing = 2
    }
}
