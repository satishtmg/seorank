﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Interfaces
{
    public interface IWebPageService
    {
        string DownloadHtml(string url);
    }
}
