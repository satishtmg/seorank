﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Rank.Queries.GetRank
{
    public class GetRankQuery : IRequest<string>
    {
        public int searchEngine { get; set; }
        public string keyword { get; set; }
    }

    public class GetSearchEnginesQueryHandler : IRequestHandler<GetRankQuery, string>
    {

        private readonly Func<string, ISearchEngineService> _searchEngineService;
        public GetSearchEnginesQueryHandler(Func<string, ISearchEngineService> searchEngineService)
        {
            this._searchEngineService = searchEngineService;
        }

        public async Task<string> Handle(GetRankQuery request, CancellationToken cancellationToken)
        {
            try
            {
                string SearchEngine = ((SearchEngineType)request.searchEngine).ToString();
                if (String.IsNullOrEmpty(SearchEngine))
                {
                    throw new Exception("Invalid data supplied!");
                }
                return _searchEngineService(SearchEngine.ToLower()).CountRank(request.keyword);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
