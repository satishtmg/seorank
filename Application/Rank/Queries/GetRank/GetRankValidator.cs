﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Rank.Queries.GetRank
{
    public class GetRankValidator : AbstractValidator<GetRankQuery>
    {
        public GetRankValidator()
        {
            RuleFor(v => v.searchEngine)
                .NotNull()
                .NotEmpty();
            RuleFor(v => v.keyword)
                .NotNull()
                .NotEmpty();
        }
    }
}
