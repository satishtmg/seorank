﻿using AutoMapper;
using Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SearchEngine.Queries.GetSearchEngines
{
    public class GetSearchEnginesQuery : IRequest<List<SearchEngineDto>>
    {
    }

    public class GetSearchEnginesQueryHandler : IRequestHandler<GetSearchEnginesQuery, List<SearchEngineDto>>
    {
        private readonly IMapper _mapper;

        public GetSearchEnginesQueryHandler(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<List<SearchEngineDto>> Handle(GetSearchEnginesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return Enum.GetValues(typeof(SearchEngineType))
                    .Cast<SearchEngineType>()
                    .Select(p => new SearchEngineDto { Id = (int)p, Name = p.ToString() })
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
