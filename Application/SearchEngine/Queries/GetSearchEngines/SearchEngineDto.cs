﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.SearchEngine.Queries.GetSearchEngines
{
    public class SearchEngineDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
