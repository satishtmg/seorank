﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Rank.Queries.GetRank;
using Application.SearchEngine.Queries.GetSearchEngines;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SEORank.Web.Controllers
{
    public class SearchEngineController : ApiController
    {
        public async Task<ActionResult<List<SearchEngineDto>>> Get()
        {
            return await Mediator.Send(new GetSearchEnginesQuery());
        }
        [HttpGet("{searchEngine}/{keyword}")]
        public async Task<ActionResult<string>> SearchRank(int searchEngine, string keyword)
        {
            return await Mediator.Send(new GetRankQuery { searchEngine = searchEngine, keyword = keyword });
        }
    }
}
