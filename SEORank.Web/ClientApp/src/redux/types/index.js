export const GET_SEARCH_ENGINES = 'GET_SEARCH_ENGINES';
export const GET_SEARCH_RESULT = 'GET_SEARCH_RESULT';
export const SEARCH_RESULT_LOADING = 'SEARCH_RESULT_LOADING';