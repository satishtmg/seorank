import React, { Component } from "react";
import FormGroup from "components/form";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import isEmpty from "helpers/isEmpty";
import { InputValidator } from "helpers/inputValidator";
import SearchResult from "./SearchResults";
import { connect } from "react-redux";
import { getSearchEngines, getSearchResults } from "../redux/actions";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

class Home extends Component {
  static displayName = Home.name;
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        searchEngine: {},
        searchText: "online title search",
      },
      errors: {},
      isFormSubmitted: false,
    };
  }
  async componentDidMount() {
    await this.props.getSearchEngines();
  }
  handleFieldChange = (name, newValue) => {
    this.setState((prevState) => ({
      ...prevState,
      formState: {
        ...prevState.formState,
        [name]: newValue,
      },
      errors: {
        ...prevState.errors,
        [name]: this.state.isFormSubmitted
          ? isEmpty(newValue)
            ? "Required"
            : ""
          : "",
      },
    }));
  };
  async handleSubmit(event) {
    // const form = event.currentTarget;
    // console.log(form);
    // event.preventDefault();
    // event.stopPropagation();
    const { searchEngine, searchText } = this.state.formState;
    let fieldsToValidate = {
      searchEngine,
      searchText,
    };
    let errors = await InputValidator(document, fieldsToValidate);
    console.log(errors);
    if (isEmpty(errors)) {
      await this.props.getSearchResults({
        searchEngine: searchEngine,
        searchText,
      });
    }
    this.setState({ errors, isFormSubmitted: true });
  }
  render() {
    const { formState, errors } = this.state;
    const { searchEngine, searchText } = formState;
    return (
      <div>
        <Card variant="outlined">
          <CardContent>
            <form className="flex items-end">
              <FormGroup
                formName="reactselect"
                onChange={this.handleFieldChange}
                name="searchEngine"
                value={searchEngine}
                options={this.props.searchEngines}
                placeholder="Select a Search Engine"
                validators={["required"]}
                error={errors && errors.searchEngine}
                //width="250px"
                label="Search Engine"
                // loading={
                //   leaveRequestTypes.loading && leaveRequestDetails.loading
                // }
                // loadingType="skeleton"
                // required
                width="250px"
              />
              <FormGroup
                name="searchText"
                value={searchText}
                onChange={this.handleFieldChange}
                label="Search Text"
                error={errors && errors.searchText}
                width="250px"
                validators={["required"]}
                placeholder="Type anything..."
                className="ml-md"
                disabled="disabled"
              />
              <CardActions className="ml-md items-end">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleSubmit()}
                >
                  Submit
                </Button>
              </CardActions>
            </form>
          </CardContent>
        </Card>
        <div className="mt-md">
          <SearchResult />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  searchEngines: state.mainReducer.searchEngines,
});

const mapDispatchToProps = {
  getSearchEngines,
  getSearchResults,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
